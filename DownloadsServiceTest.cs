﻿using NUnit.Framework;
using SitefinityWebApp.Services.Content.Downloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.JustMock;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace SitefinityWebApp.Tests
{
    [TestFixture]
    public class DownloadsServiceTest
    {
        private IDownloadsService _downloadsService;
        private static Guid DOWNLOAD_GUID;


        [SetUp]
        public void Setup()
        {
            var libraryMock = Mock.Create<LibrariesManager>();

            DOWNLOAD_GUID = Guid.NewGuid();

            _downloadsService = new DownloadsService(libraryMock);

            Mock.Arrange(() => libraryMock.GetDocuments()).Returns(new List<Document>()
            {
                new Document("ZamundaDocument1", Guid.NewGuid())
                {
                    Status = Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master
                },
                new Document("ZamundaDocument2", Guid.NewGuid())
                {
                    Status = Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master
                },
                new Document("ZamundaDocument3", Guid.NewGuid())
                {
                    Status = Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master
                },
            }.AsQueryable()).OccursOnce();

            Mock.Arrange(() => libraryMock.GetDocument(DOWNLOAD_GUID)).Returns(new Document("ZamundaDocument", DOWNLOAD_GUID)).OccursOnce();
        }



        [Test]
        public void TestGetDownloads()
        {
            var documents = _downloadsService.GetDownloadUrls();

            Assert.IsTrue(documents.Where(c => c.ApplicationName.StartsWith("ZamundaDocument")).Count() == 3);
            Assert.IsTrue(documents.Count() == 3);
        }

        [Test]
        public void TestGetDownload()
        {
            var document = _downloadsService.GetDownloadUrlById(DOWNLOAD_GUID);

            Assert.NotNull(document);
            Assert.IsTrue(document.ApplicationName == "ZamundaDocument");
        }
    }
}
