﻿using NUnit.Framework;
using SitefinityWebApp.Services.Content.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.JustMock;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SitefinityWebApp.Tests
{
    [TestFixture]
    public class CategoryServiceTests
    {
        private ICategoryService _categoryService;
        private static Guid TAXON_GUID;


        [SetUp]
        public void SetUp()
        {
            var taxonomyMock = Mock.Create<TaxonomyManager>();

            var taxonModelMock = Mock.Create<Taxonomy>();

            TAXON_GUID = Guid.NewGuid();

            _categoryService = new CategoryService(taxonomyMock);


            Mock.Arrange(() => taxonModelMock.Name).Returns("Categories");

            Mock.Arrange(() => taxonomyMock.GetTaxa<HierarchicalTaxon>()).Returns(new List<HierarchicalTaxon>()
            {
                new HierarchicalTaxon("Zamunda1", Guid.NewGuid()) {
                    Taxonomy = taxonModelMock
                },
                new HierarchicalTaxon("Zamunda2", Guid.NewGuid()){
                    Taxonomy = taxonModelMock
                },
                new HierarchicalTaxon("Zamunda3", Guid.NewGuid()){
                    Taxonomy = taxonModelMock
                },
            }.AsQueryable()).OccursOnce();

            Mock.Arrange(() => taxonomyMock.GetTaxon<HierarchicalTaxon>(TAXON_GUID)).Returns(new HierarchicalTaxon("Zamunda1", Guid.NewGuid())).OccursOnce();
        }

        [Test]
        public void TestGetCategories()
        {
            var categories = _categoryService.GetCategories();

            Assert.IsTrue(categories.Where(c => c.ApplicationName.StartsWith("Zamunda")).Count() == 3);

            Assert.IsTrue(categories.Count() == 3);
        }

        [Test]
        public void TestGetCategory()
        {
            var category = _categoryService.GetCategoryById(TAXON_GUID);

            Assert.NotNull(category);
            Assert.IsTrue(category.ApplicationName == "Zamunda1");
        }

    }
}
