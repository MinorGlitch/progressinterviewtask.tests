﻿using NUnit.Framework;
using SitefinityWebApp.Services.Content.Images;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.JustMock;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace SitefinityWebApp.Tests
{
    [TestFixture]
    public class ImagesServiceTests
    {
        private IImagesService _imagesService;
        private static Guid IMAGE_GUID;


        [SetUp]
        public void SetUp()
        {
            var libraryMock = Mock.Create<LibrariesManager>();

            IMAGE_GUID = Guid.NewGuid();


            _imagesService = new ImagesService(libraryMock);


            Mock.Arrange(() => libraryMock.GetImages()).Returns(new List<Image>()
            {
                new Image("ZamundaImage1", Guid.NewGuid())
                {
                    Status = Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master
                },
                new Image("ZamundaImage2", Guid.NewGuid())
                {
                    Status = Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master
                },
                new Image("ZamundaImage3", Guid.NewGuid())
                {
                    Status = Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master
                },
            }.AsQueryable()).OccursOnce();

            Mock.Arrange(() => libraryMock.GetImage(IMAGE_GUID)).Returns(new Image("ZamundaImage", IMAGE_GUID)).OccursOnce();
        }

        [Test]
        public void TestGetImages()
        {
            var images = _imagesService.GetImages();

            Assert.IsTrue(images.Where(c => c.ApplicationName.StartsWith("ZamundaImage")).Count() == 3);
            Assert.IsTrue(images.Count() == 3);
        }

        [Test]
        public void TestGetImage()
        {
            var image = _imagesService.GetImageById(IMAGE_GUID);

            Assert.NotNull(image);
            Assert.IsTrue(image.ApplicationName == "ZamundaImage");
        }

    }
}
